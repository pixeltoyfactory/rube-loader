package
{
    import flash.display.Sprite;
    import flash.display.StageQuality;

    import flash.events.Event;

    import net.hires.debug.Stats;


    import com.bit101.components.ComboBox;

    [SWF(frameRate="60", width="1024", height="768", backgroundColor="0x333333")]
    public class TestBed extends Sprite
    {

        private var testScene:Sprite;
        private var comboBox:ComboBox;
        public function TestBed()
        {
            super();

            //The box2d debug render can be pretty slow
            stage.quality = StageQuality.LOW;
            testScene = new ClockTest();
            addChild(testScene);

            var labels:Array = [
                "Clock",
                "Walker",
                "Joint Test",
                "Tank",
                "Truck",
                "Image Test"
                ];

            comboBox = new ComboBox(this, 100, 25, "Clock", labels);
            comboBox.addEventListener("select", selectHandler);
            addChild(comboBox);

            addChild(new Stats());
        }

        private function selectHandler(event:Event):void
        {
            removeChild(testScene);
            switch(comboBox.selectedItem) {
                case "Clock":
                    testScene = new ClockTest();
                    break;
                case "Walker":
                    testScene = new WalkerTest();
                    break;
                case "Joint Test":
                    testScene = new JointType();
                    break;
                case "Truck":
                    testScene = new TruckTest();
                    break;
                case "Image Test":
                    testScene = new ImageTest();
                    break;
                case "Tank":
                    testScene = new TankTest();
                    break;
            }
            addChildAt(testScene, 0);
        }
    }
}
