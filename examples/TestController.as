package
{
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;

    import flash.geom.Point;

    import com.pixeltoyfactory.rube.RubeLoader;

    import Box2D.Collision.b2AABB;

    import Box2D.Common.Math.b2Vec2;

    import Box2D.Dynamics.b2Body;
    import Box2D.Dynamics.b2BodyDef;
    import Box2D.Dynamics.b2DebugDraw;
    import Box2D.Dynamics.b2Fixture;
    import Box2D.Dynamics.b2World;

    import Box2D.Dynamics.Joints.b2Joint;
    import Box2D.Dynamics.Joints.b2MouseJoint;
    import Box2D.Dynamics.Joints.b2MouseJointDef;
    import Box2D.Dynamics.Joints.b2RevoluteJoint;

    public class TestController extends Sprite
    {
        protected var rubeLoader:RubeLoader;
        protected var world:b2World;
        protected var sceneJson:String;
        protected var worldScale:Number = 1;


        private var fixedStepSeconds:Number = 1/60
        private var fixedStep:Number = fixedStepSeconds * 1000.0;

        private var debugDraw:b2DebugDraw;
        protected var centerOn:b2Body;

        protected var drawDebug:Boolean = true;


        private var leftOverTime:Number = 0;

        private var lastTime:Number = 0;
        private var worldMouse:b2Vec2 = new b2Vec2();

        private var fixtureUnderMouse:b2Fixture;

        private var mouseJointGroundBody:b2Body;
        private var mouseJoint:b2MouseJoint;

        public function TestController()
        {
            super();
            worldScale = getWorldScale();

            init();


            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
            addEventListener(Event.REMOVED_FROM_STAGE, removedToStageHandler);
        }

        protected function getWorldScale():Number
        {
            return 1;
        }

        private function addMouseListeners():void
        {
            stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
        }

        private function mouseDownHandler(event:MouseEvent):void
        {
            stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
            stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);


            var pos:Point = new Point(event.stageX, event.stageY);
            pos = globalToLocal(pos);
            startMouseJoint(pos.x / worldScale, pos.y / worldScale);
        }

        private function mouseUpHandler(event:MouseEvent):void
        {
            stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
            stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);

            if (mouseJoint !== null) {
                world.DestroyJoint(mouseJoint);
                mouseJoint = null;
            }

        }

        private function mouseMoveHandler(event:MouseEvent):void
        {


            if (mouseJoint !== null) {

                var pos:Point = new Point(event.stageX, event.stageY);
                pos = globalToLocal(pos);
                var target:b2Vec2 = new b2Vec2(pos.x / worldScale, pos.y / worldScale);
                mouseJoint.SetTarget(target);
            }

        }

        private function startMouseJoint(x:Number, y:Number):void
        {
            fixtureUnderMouse = null;
            worldMouse.Set(x, y);

            var aabb:b2AABB = new b2AABB();
            var size:Number = 0.001;

            aabb.lowerBound.Set(x - size, y - size);
            aabb.upperBound.Set(x + size, y + size);

            world.QueryAABB(mouseDownQueryCallback, aabb);

            if (fixtureUnderMouse !== null) {
                var mouseJointDef:b2MouseJointDef = new b2MouseJointDef();
                mouseJointDef.bodyA = mouseJointGroundBody;
                mouseJointDef.bodyB = fixtureUnderMouse.GetBody();
                mouseJointDef.target.Set(x, y);
                mouseJointDef.maxForce = 1000 * mouseJointDef.bodyB.GetMass();
                mouseJointDef.collideConnected = true;

                mouseJoint = b2MouseJoint(world.CreateJoint(mouseJointDef));
                mouseJointDef.bodyB.SetAwake(true);
            } else {
                mouseJoint = null;
            }
        }

        private function mouseDownQueryCallback(fixture:b2Fixture):Boolean
        {
            if (fixture.TestPoint(worldMouse)) {
                fixtureUnderMouse = fixture;
                return false;
            }

            return true;
        }

        private function init():void
        {
            scaleY = -1;

            loadLevel();

            mouseJointGroundBody = world.CreateBody(new b2BodyDef());
        }

        private function removedToStageHandler(event:Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);

            stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
            stage.removeEventListener(KeyboardEvent.KEY_UP, keyUpHandler);

            stage.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
            removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
        }

        private function addedToStageHandler(event:Event):void
        {
            trace("help", drawDebug);
            if (drawDebug) {
                initDebugDraw();
            }

            removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
            addMouseListeners();

            setup();
            stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
            stage.addEventListener(KeyboardEvent.KEY_UP, keyUpHandler);

            lastTime = new Date().getTime();
            addEventListener(Event.ENTER_FRAME, enterFrameHandler);
        }

        protected function setup():void
        {
        }

        private function keyDownHandler(event:KeyboardEvent):void
        {
            if (event.keyCode == 88) {//x
                //this.scaleX *= 1.1;
                //this.scaleY *= 1.1;
                worldScale *= 1.1;
                debugDraw.SetDrawScale(worldScale);
            } else if (event.keyCode == 90) {//z
                //this.scaleX /= 1.1;
                //this.scaleY /= 1.1;
                worldScale /= 1.1;
                debugDraw.SetDrawScale(worldScale);
            }
            keyDownSub(event);
        }

        protected function keyDownSub(event:KeyboardEvent):void
        {
        }

        private function keyUpHandler(event:KeyboardEvent):void
        {
            keyUpSub(event);
        }

        protected function keyUpSub(event:KeyboardEvent):void
        {
        }

        private function enterFrameHandler(event:Event):void
        {
            update();
        }

        private function update():void
        {
            handleInput();

            stepPhysics();

            draw();
            render();

            centerCamera();
        }

        protected function render():void
        {

        }

        protected function handleInput():void
        {
            //Override in sub classes
        }

        private function draw():void
        {
            world.DrawDebugData();
        }

        private function stepPhysics():void
        {
            var currentTime:Number = new Date().getTime();
            var deltaTime:Number = (currentTime - lastTime);
            lastTime = currentTime;

            deltaTime += leftOverTime;

            while (deltaTime >= fixedStep) {

                world.Step(fixedStepSeconds, 10, 6);
                world.ClearForces();

                deltaTime -= fixedStep;
            }

            leftOverTime = deltaTime;
        }

        private function centerCamera():void
        {
            var centerPoint:b2Vec2 = getCenter();

            x = ((stage.stageWidth) / 2) - centerPoint.x;
            y = ((stage.stageHeight) / 2) + centerPoint.y;
        }

        protected function getCenter():b2Vec2
        {
            return new b2Vec2(
                centerOn.GetWorldCenter().x * worldScale,
                centerOn.GetWorldCenter().y * worldScale
            );
        }

        private function loadLevel():void
        {
            var rubeData:Object = JSON.parse(sceneJson);
            rubeLoader = new RubeLoader();

            world = rubeLoader.loadWorldFromRube(rubeData);
            rubeLoader.loadSceneIntoWorld(rubeData, world);
        }

        private function initDebugDraw():void
        {
            debugDraw = new b2DebugDraw();

            var debugSprite:Sprite = new Sprite();
            addChild(debugSprite);

            debugDraw.SetSprite(debugSprite);
            debugDraw.SetDrawScale(worldScale);
            debugDraw.SetFillAlpha(0.5);

            debugDraw.SetFlags(
                b2DebugDraw.e_shapeBit
            );

            world.SetDebugDraw(debugDraw);
        }
    }
}
