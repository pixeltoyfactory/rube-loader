package
{
    import Box2D.Common.Math.b2Vec2;

    public class JointType extends TestController
    {
        [Embed(source = 'jointTypes-min.json', mimeType='application/octet-stream')]
        private var JointTypes:Class;

        private var moveFlags:Number = 0;

        public function JointType()
        {
            sceneJson = String(new JointTypes());
            super();
        }

        override protected function getCenter():b2Vec2
        {
            return new b2Vec2(
                1.392 * getWorldScale(),
                3.206 * getWorldScale()
            );
        }

        override protected function getWorldScale():Number
        {
            return 45;

        }
    }
}

