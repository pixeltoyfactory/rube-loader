package
{
    import Box2D.Dynamics.b2Body;
    import Box2D.Dynamics.Joints.b2Joint;
    import Box2D.Dynamics.Joints.b2RevoluteJoint;

    public class TankTest extends VehicleTest
    {
        [Embed(source = 'tank-min.json', mimeType='application/octet-stream')]
        private var TankJson:Class;

        private var wheelBodies:Vector.<b2Body>;

        public function TankTest()
        {
            sceneJson = String(new TankJson());
            super();
        }

        override protected function getWorldScale():Number
        {
            return 32;
        }

        override protected function setup():void
        {
            wheelBodies = rubeLoader.getNamedBodies(world, "tankwheel");
            var centerOnName:String = "tankchassis";
            centerOn = rubeLoader.getNamedBodies(world, centerOnName)[0];
        }

        override protected function updateMotorSpeed():void
        {
            var speed:Number = 0;
            if ((moveFlags & MOVE_LEFT) === MOVE_LEFT) {
                speed = 10;
            } else if ((moveFlags & MOVE_RIGHT) === MOVE_RIGHT) {
                speed = -10;
            }

            for (var i:int = 0; i < wheelBodies.length; i++) {
                var wheelBody:b2Body = wheelBodies[i];
                wheelBody.SetAngularVelocity(speed);
            }
        }
    }
}
