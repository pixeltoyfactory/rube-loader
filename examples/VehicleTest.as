package
{
    import flash.events.KeyboardEvent;

    public class VehicleTest extends TestController
    {
        protected const MOVE_LEFT:Number = 0x01;
        protected const MOVE_RIGHT:Number = 0x02;

        protected const MOVE_LEFT_KEY_CODE:Number = 74; //j
        protected const MOVE_RIGHT_KEY_CODE:Number = 75; //k

        protected var moveFlags:Number = 0;

        public function VehicleTest()
        {
            super();
        }

        override protected function keyDownSub(event:KeyboardEvent):void
        {
            if (event.keyCode === MOVE_LEFT_KEY_CODE) {
                this.moveFlags |= MOVE_LEFT;
            } else if (event.keyCode === MOVE_RIGHT_KEY_CODE) {
                this.moveFlags |= MOVE_RIGHT;
            }
        }

        override protected function keyUpSub(event:KeyboardEvent):void
        {
            if (event.keyCode === MOVE_LEFT_KEY_CODE) {
                this.moveFlags &= ~MOVE_LEFT;
            } else if (event.keyCode === MOVE_RIGHT_KEY_CODE) {
                this.moveFlags &= ~MOVE_RIGHT;
            }
        }

        override protected function handleInput():void
        {
            updateMotorSpeed();
        }

        protected function updateMotorSpeed():void
        {
        }
    }
}

