package
{
    import Box2D.Common.Math.b2Vec2;

    public class ClockTest extends TestController
    {
        [Embed(source = 'clock-min.json', mimeType='application/octet-stream')]
        private var ClockJson:Class;

        private var moveFlags:Number = 0;

        public function ClockTest()
        {
            sceneJson = String(new ClockJson());
            super();
        }

        override protected function getCenter():b2Vec2
        {
            return new b2Vec2(
                19.978 * getWorldScale(),
                -5.239 * getWorldScale()
            );
        }

        override protected function getWorldScale():Number
        {
            return 12.34;
        }
    }
}
