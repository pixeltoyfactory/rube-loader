package
{
    import Box2D.Dynamics.Joints.b2Joint;
    import Box2D.Dynamics.Joints.b2RevoluteJoint;

    public class WalkerTest extends VehicleTest
    {

        [Embed(source = 'walker-min.json', mimeType='application/octet-stream')]
        private var WalkerJson:Class;

        private var walkerJoints:Vector.<b2Joint>;


        public function WalkerTest()
        {
            sceneJson = String(new WalkerJson());
            super();
        }

        override protected function getWorldScale():Number
        {
            return 38;
        }

        override protected function setup():void
        {
            walkerJoints = rubeLoader.getNamedJoints(world, "walkerjoint");
            var centerOnName:String = "walkerchassis";
            centerOn = rubeLoader.getNamedBodies(world, centerOnName)[0];
        }

        override protected function updateMotorSpeed():void
        {
            var jointSpeed:Number = 0;
            if ((moveFlags & MOVE_LEFT) === MOVE_LEFT) {
                jointSpeed = -5;
            } else if ((moveFlags & MOVE_RIGHT) === MOVE_RIGHT) {
                jointSpeed = 5;
            }

            for (var i:int = 0; i < walkerJoints.length; i++) {
                var joint:b2RevoluteJoint = b2RevoluteJoint(walkerJoints[i]);
                joint.SetMotorSpeed(jointSpeed);
            }
        }

    }
}
