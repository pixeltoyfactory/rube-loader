package
{
    import flash.events.KeyboardEvent;

    import com.pixeltoyfactory.rube.RubeRenderer;

    import Box2D.Common.Math.b2Vec2;

    import Box2D.Dynamics.b2Body;
    import Box2D.Dynamics.Joints.b2Joint;
    import Box2D.Dynamics.Joints.b2RevoluteJoint;

    public class ImageTest extends VehicleTest
    {
        [Embed(source = 'image_test-min.json', mimeType='application/octet-stream')]
        private var ImageTestJson:Class;

        private var wheelBodies:Vector.<b2Body>;

        private var renderer:RubeRenderer;

        public function ImageTest()
        {
            sceneJson = String(new ImageTestJson());
            super();
        }

        override protected function render():void
        {
            renderer.render();
        }

        override protected function getCenter():b2Vec2
        {
            return new b2Vec2(
                -0.665, 3.318
            );
        }

        override protected function getWorldScale():Number
        {
            return 28.43;
        }

        override protected function setup():void
        {
            wheelBodies = rubeLoader.getNamedBodies(world, "truckwheel");

            renderer = new RubeRenderer(this, rubeLoader.rubeImages, getWorldScale());
            addChild(renderer);
        }

        override protected function keyDownSub(event:KeyboardEvent):void
        {
            super.keyDownSub(event);
            if (event.keyCode == 88) {//x
                renderer.scaleX *= 1.1;
                renderer.scaleY *= 1.1;
            } else if (event.keyCode == 90) {//z
                renderer.scaleX /= 1.1;
                renderer.scaleY /= 1.1;
            }
        }

        override protected function updateMotorSpeed():void
        {
            var speed:Number = 0;
            if ((moveFlags & MOVE_LEFT) === MOVE_LEFT) {
                speed = 20;
            } else if ((moveFlags & MOVE_RIGHT) === MOVE_RIGHT) {
                speed = -20;
            }


            for (var i:int = 0; i < wheelBodies.length; i++) {
                var wheelBody:b2Body = wheelBodies[i];
                wheelBody.SetAngularVelocity(speed);
            }
        }

    }
}


