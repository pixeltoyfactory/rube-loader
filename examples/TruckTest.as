package
{
    import Box2D.Dynamics.b2Body;
    import Box2D.Dynamics.Joints.b2Joint;
    import Box2D.Dynamics.Joints.b2RevoluteJoint;

    public class TruckTest extends VehicleTest
    {
        [Embed(source = 'truck.json', mimeType='application/octet-stream')]
        private var TruckJson:Class;

        private var wheelBodies:Vector.<b2Body>;

        public function TruckTest()
        {
            sceneJson = String(new TruckJson());
            super();
        }

        override protected function getWorldScale():Number
        {
            return 22;
        }

        override protected function setup():void
        {
            wheelBodies = rubeLoader.getNamedBodies(world, "truckwheel");
            var centerOnName:String = "truckchassis";
            centerOn = rubeLoader.getNamedBodies(world, centerOnName)[0];
        }

        override protected function updateMotorSpeed():void
        {
            var speed:Number = 0;
            if ((moveFlags & MOVE_LEFT) === MOVE_LEFT) {
                speed = 20;
            } else if ((moveFlags & MOVE_RIGHT) === MOVE_RIGHT) {
                speed = -20;
            }

            for (var i:int = 0; i < wheelBodies.length; i++) {
                var wheelBody:b2Body = wheelBodies[i];
                wheelBody.SetAngularVelocity(speed);
            }
        }
    }
}
