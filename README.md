Rube Loader
---

A library to help with loading files created with R.U.B.E https://www.iforce2d.net/rube/ into actionscript projects.

Demo
===
[Try the demo](http://www.pixeltoyfactory.com/rube/)